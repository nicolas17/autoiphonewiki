FROM python:3.9

RUN apt-get update && apt-get install -y diffstat && rm -rf /var/lib/apt/lists
RUN curl -sSLf https://install.python-poetry.org | python3 - --version 1.1.14
ENV PATH=/root/.local/bin:$PATH
