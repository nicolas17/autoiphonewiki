<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/ipadair16.jinja -->
{{nobots}}
== [[iPad Air (3rd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Baseband
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 16.1
| 20B82
| [[Keys:SydneyB 20B82 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyB 20B82 (iPad11,4)|iPad11,4]]
| rowspan="5" | 5.01.01
| {{date|2022|10|24}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-92863/34E84A2C-317A-4103-96C8-62D07C6E59EA/iPad_Spring_2019_16.1_20B82_Restore.ipsw iPad_Spring_2019_16.1_20B82_Restore.ipsw]
| <code>2ac145558b12284067a1ba7b1b04b31494691284</code>
| 5,892,799,884
| [https://updates.cdn-apple.com/2022FallFCS/documentation/012-87561/D3B93483-7C2B-496B-A5F0-6C8357E25AEC/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#161 Release Notes]<br/>[https://support.apple.com/HT213489 Security Notes]
|-
| 16.1.1
| 20B101
| [[Keys:SydneyB 20B101 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyB 20B101 (iPad11,4)|iPad11,4]]
| {{date|2022|11|09}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-97426/1F43D991-0591-447D-B7F9-493913C6036A/iPad_Spring_2019_16.1.1_20B101_Restore.ipsw iPad_Spring_2019_16.1.1_20B101_Restore.ipsw]
| <code>23fcfdd4c7c5ea6fd541edebce145be29c80c53f</code>
| 5,891,923,189
| [https://updates.cdn-apple.com/2022FallFCS/documentation/032-01628/ECB0AF09-F048-4300-AEBB-3EF113A2A152/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1611 Release Notes]<br/>[https://support.apple.com/HT213505 Security Notes]
|-
| 16.2
| 20C65
| [[Keys:SydneyC 20C65 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyC 20C65 (iPad11,4)|iPad11,4]]
| {{date|2022|12|13}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/032-14019/7D857460-A49F-4175-8C31-C884BE486FEA/iPad_Spring_2019_16.2_20C65_Restore.ipsw iPad_Spring_2019_16.2_20C65_Restore.ipsw]
| <code>2469f458b07da38ec9fb1e42faf59c70c526936b</code>
| 5,966,142,597
| [https://updates.cdn-apple.com/2022WinterFCS/documentation/032-11205/0F20888C-FB9C-44D0-84FF-50405AF8AD38/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#162 Release Notes]<br/>[https://support.apple.com/HT213530 Security Notes]
|-
| 16.3
| 20D47
| [[Keys:SydneyD 20D47 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyD 20D47 (iPad11,4)|iPad11,4]]
| {{date|2023|01|23}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-35847/9C1F82F0-350F-4223-8520-72784FF12B3A/iPad_Spring_2019_16.3_20D47_Restore.ipsw iPad_Spring_2019_16.3_20D47_Restore.ipsw]
| <code>4afef236d323a085f31c2ad0dcac1d2c2ecca74d</code>
| 5,941,188,446
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-35610/19B8E936-C7C5-4BAD-8EB6-0FDA92110906/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#163 Release Notes]<br/>[https://support.apple.com/HT213606 Security Notes]
|-
| 16.3.1
| 20D67
| [[Keys:SydneyD 20D67 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyD 20D67 (iPad11,4)|iPad11,4]]
| {{date|2023|02|13}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-49698/5DBACB3D-0AF7-4535-BD2A-462C0AF7B0F8/iPad_Spring_2019_16.3.1_20D67_Restore.ipsw iPad_Spring_2019_16.3.1_20D67_Restore.ipsw]
| <code>9558007480354678d9aec03a87aa0bbc90446822</code>
| 5,941,610,952
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-44522/631E8840-0A15-4AF2-95BB-DFF58DC5DBDE/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1631 Release Notes]<br/>[https://support.apple.com/HT213635 Security Notes]
|-
| 16.4
| 20E246
| [[Keys:SydneyE 20E246 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyE 20E246 (iPad11,4)|iPad11,4]]
| rowspan="4" | 5.02.02
| {{date|2023|03|27}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-66063/C442A2B4-162A-4B4D-BE23-6D0E488AFAD9/iPad_Spring_2019_16.4_20E246_Restore.ipsw iPad_Spring_2019_16.4_20E246_Restore.ipsw]
| <code>bc55ccbc7231ceb2f6f887db6f9cade5ce2521d4</code>
| 6,170,090,534
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-66504/9BBF6487-5030-4D3D-8E33-343EFC565EA9/iPadLongRTF.ipd iPadLongRTF.ipd]<br/>[https://support.apple.com/HT213408#164 Release Notes]<br/>[https://support.apple.com/HT213676 Security Notes]
|-
| 16.4.1
| 20E252
| [[Keys:SydneyE 20E252 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyE 20E252 (iPad11,4)|iPad11,4]]
| {{date|2023|04|07}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-71041/FB39D81A-6A46-4004-80E6-DD70BE7BB8C1/iPad_Spring_2019_16.4.1_20E252_Restore.ipsw iPad_Spring_2019_16.4.1_20E252_Restore.ipsw]
| <code>9c5d89277ee42b81d42521397aa5ae40d393d139</code>
| 6,170,099,802
| [https://updates.cdn-apple.com/2023FallFCS/documentation/032-71843/93857F06-5B4D-477F-8C96-E42884FA9BA2/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1641 Release Notes]<br/>[https://support.apple.com/HT213720 Security Notes]
|-
| 16.5
| 20F66
| [[Keys:SydneyF 20F66 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyF 20F66 (iPad11,4)|iPad11,4]]
| {{date|2023|05|18}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-85323/1C25C1EC-DE75-42D0-BC1E-E8E105336E96/iPad_Spring_2019_16.5_20F66_Restore.ipsw iPad_Spring_2019_16.5_20F66_Restore.ipsw]
| <code>c7ec77ffc9734e7d785372fb3f9c68835125e8b4</code>
| 6,184,456,920
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/032-82421/764434EB-0DD4-4450-95D2-EE204BDD6FC0/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#165 Release Notes]<br/>[https://support.apple.com/HT213757 Security Notes]
|-
| 16.5.1
| 20F75
| [[Keys:SydneyF 20F75 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyF 20F75 (iPad11,4)|iPad11,4]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/042-02784/AB148D94-9393-4D9D-A1B4-AB1D30342F47/iPad_Spring_2019_16.5.1_20F75_Restore.ipsw iPad_Spring_2019_16.5.1_20F75_Restore.ipsw]
| <code>87085934d894f13e56f68ee7fe003cd49e1c9df4</code>
| 6,184,343,471
| [https://updates.cdn-apple.com/2023SpringFCS/documentation/032-98218/E5421AB8-87FE-4F61-9DA5-0DE3D2C8F070/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1651 Release Notes]<br/>[https://support.apple.com/HT213814 Security Notes]
|-
| 16.6
| 20G75
| [[Keys:SydneyG 20G75 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyG 20G75 (iPad11,4)|iPad11,4]]
| rowspan="5" | 5.03.01
| {{date|2023|07|24}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-17805/A881EEF0-164A-46DC-8E1C-3BD544F49C06/iPad_Spring_2019_16.6_20G75_Restore.ipsw iPad_Spring_2019_16.6_20G75_Restore.ipsw]
| <code>bfe6364ae0e1b80685813266f1364720da6842ca</code>
| 6,186,669,429
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-12553/4310BC98-5959-469F-B85F-8536F5F50617/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#166 Release Notes]<br/>[https://support.apple.com/HT213841 Security Notes]
|-
| 16.6.1
| 20G81
| [[Keys:SydneyG 20G81 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneyG 20G81 (iPad11,4)|iPad11,4]]
| {{date|2023|09|07}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-44566/EAC342DA-468C-46C9-A595-180304491598/iPad_Spring_2019_16.6.1_20G81_Restore.ipsw iPad_Spring_2019_16.6.1_20G81_Restore.ipsw]
| <code>1cf31221ac5e7cf727bbb6234f4281c4fcbfbf79</code>
| 6,187,261,370
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-43505/F095284C-B069-416B-BBB7-2B44AD2B9212/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213407#1661 Release Notes]<br/>[https://support.apple.com/HT213905 Security Notes]
|-
| 16.7
| 20H19
| [[Keys:SydneySecurityDawn 20H19 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneySecurityDawn 20H19 (iPad11,4)|iPad11,4]]
| {{date|2023|09|21}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-43538/E9BC2E32-0A40-40B8-92BC-95AB9C4F15F3/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#167 Release Notes]<br/>[https://support.apple.com/HT213927 Security Notes]
|-
| 16.7.1
| 20H30
| [[Keys:SydneySecurityDawn 20H30 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneySecurityDawn 20H30 (iPad11,4)|iPad11,4]]
| {{date|2023|10|10}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-41314/7232F047-A81B-496C-9415-F2F83A141646/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1671 Release Notes]<br/>[https://support.apple.com/HT213972 Security Notes]
|-
| 16.7.2
| 20H115
| [[Keys:SydneySecurityDawnB 20H115 (iPad11,3)|iPad11,3]]<br/>[[Keys:SydneySecurityDawnB 20H115 (iPad11,4)|iPad11,4]]
| {{date|2023|10|25}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-82927/4E7A8DC8-D6A1-4247-93C4-7CBBC05B303C/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1672 Release Notes]<br/>[https://support.apple.com/HT213981 Security Notes]
|}

== [[iPad Air (4th generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Baseband
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 16.1
| 20B82
| [[Keys:SydneyB 20B82 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyB 20B82 (iPad13,2)|iPad13,2]]
| rowspan="5" | 4.00.00
| {{date|2022|10|24}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-93022/880B5B84-0007-4058-973A-7BCD4C4DD222/iPad_Fall_2020_16.1_20B82_Restore.ipsw iPad_Fall_2020_16.1_20B82_Restore.ipsw]
| <code>d733c683b2d877ce69aa5c18dde37f4cf3523512</code>
| 5,770,449,935
| [https://updates.cdn-apple.com/2022FallFCS/documentation/012-87561/D3B93483-7C2B-496B-A5F0-6C8357E25AEC/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#161 Release Notes]<br/>[https://support.apple.com/HT213489 Security Notes]
|-
| 16.1.1
| 20B101
| [[Keys:SydneyB 20B101 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyB 20B101 (iPad13,2)|iPad13,2]]
| {{date|2022|11|09}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-97074/29795F49-9E4D-4D22-AF50-43F85586EA63/iPad_Fall_2020_16.1.1_20B101_Restore.ipsw iPad_Fall_2020_16.1.1_20B101_Restore.ipsw]
| <code>95b45d1aeb53543c75b821077fd90c8a8e6fb531</code>
| 5,770,241,869
| [https://updates.cdn-apple.com/2022FallFCS/documentation/032-01628/ECB0AF09-F048-4300-AEBB-3EF113A2A152/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1611 Release Notes]<br/>[https://support.apple.com/HT213505 Security Notes]
|-
| 16.2
| 20C65
| [[Keys:SydneyC 20C65 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyC 20C65 (iPad13,2)|iPad13,2]]
| {{date|2022|12|13}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/032-13969/1F280866-2A36-4A9B-ABF4-BDF75CDC5913/iPad_Fall_2020_16.2_20C65_Restore.ipsw iPad_Fall_2020_16.2_20C65_Restore.ipsw]
| <code>c40ac26b27e2080e879f1373d09a9fa39f465c27</code>
| 5,843,179,033
| [https://updates.cdn-apple.com/2022WinterFCS/documentation/032-11205/0F20888C-FB9C-44D0-84FF-50405AF8AD38/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#162 Release Notes]<br/>[https://support.apple.com/HT213530 Security Notes]
|-
| 16.3
| 20D47
| [[Keys:SydneyD 20D47 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyD 20D47 (iPad13,2)|iPad13,2]]
| {{date|2023|01|23}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-36371/B67FA6F3-8808-4F29-9D83-7F7FF5FEBBF3/iPad_Fall_2020_16.3_20D47_Restore.ipsw iPad_Fall_2020_16.3_20D47_Restore.ipsw]
| <code>b3e1745d15b132da031ee06b4ecfd71366698db6</code>
| 5,850,279,925
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-35610/19B8E936-C7C5-4BAD-8EB6-0FDA92110906/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#163 Release Notes]<br/>[https://support.apple.com/HT213606 Security Notes]
|-
| 16.3.1
| 20D67
| [[Keys:SydneyD 20D67 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyD 20D67 (iPad13,2)|iPad13,2]]
| {{date|2023|02|13}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-50068/2E887141-268E-4C34-AA0C-B2009B8D8488/iPad_Fall_2020_16.3.1_20D67_Restore.ipsw iPad_Fall_2020_16.3.1_20D67_Restore.ipsw]
| <code>2564caad77ebe15391b0ead79cdee281f33730d9</code>
| 5,850,602,681
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-44522/631E8840-0A15-4AF2-95BB-DFF58DC5DBDE/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1631 Release Notes]<br/>[https://support.apple.com/HT213635 Security Notes]
|-
| 16.4
| 20E246
| [[Keys:SydneyE 20E246 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyE 20E246 (iPad13,2)|iPad13,2]]
| rowspan="2" | 4.01.02
| {{date|2023|03|27}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-66072/3920BEF8-A22A-47E3-BBBA-F1243E0E4A71/iPad_Fall_2020_16.4_20E246_Restore.ipsw iPad_Fall_2020_16.4_20E246_Restore.ipsw]
| <code>bef25b222d79bda9b845215703b7c3caf948013e</code>
| 6,084,456,711
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-66504/9BBF6487-5030-4D3D-8E33-343EFC565EA9/iPadLongRTF.ipd iPadLongRTF.ipd]<br/>[https://support.apple.com/HT213408#164 Release Notes]<br/>[https://support.apple.com/HT213676 Security Notes]
|-
| 16.4.1
| 20E252
| [[Keys:SydneyE 20E252 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyE 20E252 (iPad13,2)|iPad13,2]]
| {{date|2023|04|07}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-71235/221F5351-2FDB-4C0D-B79D-18F454D5489C/iPad_Fall_2020_16.4.1_20E252_Restore.ipsw iPad_Fall_2020_16.4.1_20E252_Restore.ipsw]
| <code>31d76fa2c09f1577904c41940284b2f54457e373</code>
| 6,084,794,581
| [https://updates.cdn-apple.com/2023FallFCS/documentation/032-71843/93857F06-5B4D-477F-8C96-E42884FA9BA2/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1641 Release Notes]<br/>[https://support.apple.com/HT213720 Security Notes]
|-
| 16.5
| 20F66
| [[Keys:SydneyF 20F66 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyF 20F66 (iPad13,2)|iPad13,2]]
| rowspan="2" | 4.02.01
| {{date|2023|05|18}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-85030/B6A7834D-C169-4297-9ABE-0A831F54D9BC/iPad_Fall_2020_16.5_20F66_Restore.ipsw iPad_Fall_2020_16.5_20F66_Restore.ipsw]
| <code>f4f6b4c6b2898ebcd75549d3bcb1466f9a3a6104</code>
| 6,099,378,818
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/032-82421/764434EB-0DD4-4450-95D2-EE204BDD6FC0/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#165 Release Notes]<br/>[https://support.apple.com/HT213757 Security Notes]
|-
| 16.5.1
| 20F75
| [[Keys:SydneyF 20F75 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyF 20F75 (iPad13,2)|iPad13,2]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/042-02618/7BFE8708-9EB2-431B-A201-109330D91F52/iPad_Fall_2020_16.5.1_20F75_Restore.ipsw iPad_Fall_2020_16.5.1_20F75_Restore.ipsw]
| <code>a4dc26a06423ec4e16f3a7f3988b2f81945e7fed</code>
| 6,098,879,558
| [https://updates.cdn-apple.com/2023SpringFCS/documentation/032-98218/E5421AB8-87FE-4F61-9DA5-0DE3D2C8F070/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1651 Release Notes]<br/>[https://support.apple.com/HT213814 Security Notes]
|-
| 16.6
| 20G75
| [[Keys:SydneyG 20G75 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyG 20G75 (iPad13,2)|iPad13,2]]
| rowspan="5" | 4.03.01
| {{date|2023|07|24}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-17863/D29E3351-2C8B-4F15-AF9A-EE098661183E/iPad_Fall_2020_16.6_20G75_Restore.ipsw iPad_Fall_2020_16.6_20G75_Restore.ipsw]
| <code>156f1057ef6ba5f5a9f24170212dd592cb4720e4</code>
| 6,101,540,649
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-12553/4310BC98-5959-469F-B85F-8536F5F50617/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#166 Release Notes]<br/>[https://support.apple.com/HT213841 Security Notes]
|-
| 16.6.1
| 20G81
| [[Keys:SydneyG 20G81 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneyG 20G81 (iPad13,2)|iPad13,2]]
| {{date|2023|09|07}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-44527/AE4D1B09-960F-4A30-A56F-CB0A7D1E2276/iPad_Fall_2020_16.6.1_20G81_Restore.ipsw iPad_Fall_2020_16.6.1_20G81_Restore.ipsw]
| <code>594b4e3b6dee2cf10a3a7bc3e544b021a1fd503c</code>
| 6,101,353,624
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-43505/F095284C-B069-416B-BBB7-2B44AD2B9212/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213407#1661 Release Notes]<br/>[https://support.apple.com/HT213905 Security Notes]
|-
| 16.7
| 20H19
| [[Keys:SydneySecurityDawn 20H19 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneySecurityDawn 20H19 (iPad13,2)|iPad13,2]]
| {{date|2023|09|21}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-43538/E9BC2E32-0A40-40B8-92BC-95AB9C4F15F3/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#167 Release Notes]<br/>[https://support.apple.com/HT213927 Security Notes]
|-
| 16.7.1
| 20H30
| [[Keys:SydneySecurityDawn 20H30 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneySecurityDawn 20H30 (iPad13,2)|iPad13,2]]
| {{date|2023|10|10}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-41314/7232F047-A81B-496C-9415-F2F83A141646/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1671 Release Notes]<br/>[https://support.apple.com/HT213972 Security Notes]
|-
| 16.7.2
| 20H115
| [[Keys:SydneySecurityDawnB 20H115 (iPad13,1)|iPad13,1]]<br/>[[Keys:SydneySecurityDawnB 20H115 (iPad13,2)|iPad13,2]]
| {{date|2023|10|25}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-82927/4E7A8DC8-D6A1-4247-93C4-7CBBC05B303C/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1672 Release Notes]<br/>[https://support.apple.com/HT213981 Security Notes]
|}

== [[iPad Air (5th generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Baseband
! Release Date
! Download URL
! SHA1 Hash
! File Size
! Documentation
|-
| 16.1
| 20B82
| [[Keys:SydneyB 20B82 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyB 20B82 (iPad13,17)|iPad13,17]]
| rowspan="2" | 2.12.02
| {{date|2022|10|24}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-92864/9E09645F-7818-4C27-8701-FCD74B7FA85F/iPad_Spring_2022_16.1_20B82_Restore.ipsw iPad_Spring_2022_16.1_20B82_Restore.ipsw]
| <code>174f293c26bb6a083c59ab293976f061e5f4cfdb</code>
| 5,883,736,675
| [https://updates.cdn-apple.com/2022FallFCS/documentation/012-87561/D3B93483-7C2B-496B-A5F0-6C8357E25AEC/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#161 Release Notes]<br/>[https://support.apple.com/HT213489 Security Notes]
|-
| 16.1.1
| 20B101
| [[Keys:SydneyB 20B101 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyB 20B101 (iPad13,17)|iPad13,17]]
| {{date|2022|11|09}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-97077/DF56AF85-00A6-4C2E-B8AE-B12DF9E015C9/iPad_Spring_2022_16.1.1_20B101_Restore.ipsw iPad_Spring_2022_16.1.1_20B101_Restore.ipsw]
| <code>8baf643838b32b955fca597772ceceb2328552be</code>
| 5,883,725,498
| [https://updates.cdn-apple.com/2022FallFCS/documentation/032-01628/ECB0AF09-F048-4300-AEBB-3EF113A2A152/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1611 Release Notes]<br/>[https://support.apple.com/HT213505 Security Notes]
|-
| 16.2
| 20C65
| [[Keys:SydneyC 20C65 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyC 20C65 (iPad13,17)|iPad13,17]]
| 2.21.00
| {{date|2022|12|13}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/032-14001/0F1B3030-6418-43D3-A6DC-972F78216153/iPad_Spring_2022_16.2_20C65_Restore.ipsw iPad_Spring_2022_16.2_20C65_Restore.ipsw]
| <code>53b69ae65e32851d03e480e5e2dc1972cf502575</code>
| 5,956,587,401
| [https://updates.cdn-apple.com/2022WinterFCS/documentation/032-11205/0F20888C-FB9C-44D0-84FF-50405AF8AD38/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#162 Release Notes]<br/>[https://support.apple.com/HT213530 Security Notes]
|-
| 16.3
| 20D47
| [[Keys:SydneyD 20D47 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyD 20D47 (iPad13,17)|iPad13,17]]
| rowspan="2" | 2.40.01
| {{date|2023|01|23}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-35790/0C8FBD72-AFAB-417C-8AFE-8EAD463565A6/iPad_Spring_2022_16.3_20D47_Restore.ipsw iPad_Spring_2022_16.3_20D47_Restore.ipsw]
| <code>b80f7841c1085251e2edea9d59f1835e17ecc4ed</code>
| 5,965,005,989
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-35610/19B8E936-C7C5-4BAD-8EB6-0FDA92110906/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#163 Release Notes]<br/>[https://support.apple.com/HT213606 Security Notes]
|-
| 16.3.1
| 20D67
| [[Keys:SydneyD 20D67 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyD 20D67 (iPad13,17)|iPad13,17]]
| {{date|2023|02|13}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-49908/FEE47B4A-C37D-4CE7-90AF-48E7FE9B9706/iPad_Spring_2022_16.3.1_20D67_Restore.ipsw iPad_Spring_2022_16.3.1_20D67_Restore.ipsw]
| <code>32d32c3bd00bc41834b1b40cba56bead3f293f7d</code>
| 5,964,596,299
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-44522/631E8840-0A15-4AF2-95BB-DFF58DC5DBDE/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1631 Release Notes]<br/>[https://support.apple.com/HT213635 Security Notes]
|-
| 16.4
| 20E246
| [[Keys:SydneyE 20E246 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyE 20E246 (iPad13,17)|iPad13,17]]
| rowspan="2" | 2.55.00
| {{date|2023|03|27}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-66059/325ADC04-6D77-492F-B0DB-D5D68D5BD8EA/iPad_Spring_2022_16.4_20E246_Restore.ipsw iPad_Spring_2022_16.4_20E246_Restore.ipsw]
| <code>c1bff811051ef48ab80648842f0bbb260b98ce91</code>
| 6,198,003,958
| [https://updates.cdn-apple.com/2023WinterFCS/documentation/032-66504/9BBF6487-5030-4D3D-8E33-343EFC565EA9/iPadLongRTF.ipd iPadLongRTF.ipd]<br/>[https://support.apple.com/HT213408#164 Release Notes]<br/>[https://support.apple.com/HT213676 Security Notes]
|-
| 16.4.1
| 20E252
| [[Keys:SydneyE 20E252 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyE 20E252 (iPad13,17)|iPad13,17]]
| {{date|2023|04|07}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-71233/F9F84D8F-D91B-414E-9F76-6D05DC1FBBB7/iPad_Spring_2022_16.4.1_20E252_Restore.ipsw iPad_Spring_2022_16.4.1_20E252_Restore.ipsw]
| <code>954cc492b3a2eaf0788830a4a314f0dab94965fb</code>
| 6,198,726,615
| [https://updates.cdn-apple.com/2023FallFCS/documentation/032-71843/93857F06-5B4D-477F-8C96-E42884FA9BA2/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1641 Release Notes]<br/>[https://support.apple.com/HT213720 Security Notes]
|-
| 16.5
| 20F66
| [[Keys:SydneyF 20F66 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyF 20F66 (iPad13,17)|iPad13,17]]
| rowspan="2" | 2.70.01
| {{date|2023|05|18}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-85625/80ACA4C7-8683-41C6-BD04-520607D8F76B/iPad_Spring_2022_16.5_20F66_Restore.ipsw iPad_Spring_2022_16.5_20F66_Restore.ipsw]
| <code>5b7feb4d8672f12d1eb30a5589eba3fb337018ec</code>
| 6,212,798,199
| [https://updates.cdn-apple.com/2022SpringFCS/documentation/032-82421/764434EB-0DD4-4450-95D2-EE204BDD6FC0/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#165 Release Notes]<br/>[https://support.apple.com/HT213757 Security Notes]
|-
| 16.5.1
| 20F75
| [[Keys:SydneyF 20F75 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyF 20F75 (iPad13,17)|iPad13,17]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/042-02726/C2588E30-2B79-4B46-8953-882B8D177682/iPad_Spring_2022_16.5.1_20F75_Restore.ipsw iPad_Spring_2022_16.5.1_20F75_Restore.ipsw]
| <code>6ffb69ea59af60500d0cede4cee867fc006353e7</code>
| 6,212,646,063
| [https://updates.cdn-apple.com/2023SpringFCS/documentation/032-98218/E5421AB8-87FE-4F61-9DA5-0DE3D2C8F070/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1651 Release Notes]<br/>[https://support.apple.com/HT213814 Security Notes]
|-
| 16.6
| 20G75
| [[Keys:SydneyG 20G75 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyG 20G75 (iPad13,17)|iPad13,17]]
| rowspan="5" | 2.80.01
| {{date|2023|07|24}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-17561/5848AC7B-2084-4127-8DEE-52D599C068EB/iPad_Spring_2022_16.6_20G75_Restore.ipsw iPad_Spring_2022_16.6_20G75_Restore.ipsw]
| <code>c537cefe8f1cbaaef8b13b586f212ca2e12acfb8</code>
| 6,215,595,722
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-12553/4310BC98-5959-469F-B85F-8536F5F50617/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#166 Release Notes]<br/>[https://support.apple.com/HT213841 Security Notes]
|-
| 16.6.1
| 20G81
| [[Keys:SydneyG 20G81 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneyG 20G81 (iPad13,17)|iPad13,17]]
| {{date|2023|09|07}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-43796/8D36D83D-C210-41B9-9BD9-FAB278C2D7DC/iPad_Spring_2022_16.6.1_20G81_Restore.ipsw iPad_Spring_2022_16.6.1_20G81_Restore.ipsw]
| <code>0299ebf5d54f19180b8c942e7008f899d95f80d6</code>
| 6,215,101,201
| [https://updates.cdn-apple.com/2023SummerFCS/documentation/042-43505/F095284C-B069-416B-BBB7-2B44AD2B9212/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213407#1661 Release Notes]<br/>[https://support.apple.com/HT213905 Security Notes]
|-
| 16.7
| 20H19
| [[Keys:SydneySecurityDawn 20H19 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneySecurityDawn 20H19 (iPad13,17)|iPad13,17]]
| {{date|2023|09|21}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-43538/E9BC2E32-0A40-40B8-92BC-95AB9C4F15F3/iPadShortRTF.ipd iPadShortRTF.ipd]<br/>[https://support.apple.com/HT213408#167 Release Notes]<br/>[https://support.apple.com/HT213927 Security Notes]
|-
| 16.7.1
| 20H30
| [[Keys:SydneySecurityDawn 20H30 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneySecurityDawn 20H30 (iPad13,17)|iPad13,17]]
| {{date|2023|10|10}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-41314/7232F047-A81B-496C-9415-F2F83A141646/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1671 Release Notes]<br/>[https://support.apple.com/HT213972 Security Notes]
|-
| 16.7.2
| 20H115
| [[Keys:SydneySecurityDawnB 20H115 (iPad13,16)|iPad13,16]]<br/>[[Keys:SydneySecurityDawnB 20H115 (iPad13,17)|iPad13,17]]
| {{date|2023|10|25}}
| colspan="3" {{n/a}}
| [https://updates.cdn-apple.com/2023FallFCS/documentation/042-82927/4E7A8DC8-D6A1-4247-93C4-7CBBC05B303C/iPadSydneyUpdateShortRTF.ipd iPadSydneyUpdateShortRTF.ipd]<br/>[https://support.apple.com/HT213408#1672 Release Notes]<br/>[https://support.apple.com/HT213981 Security Notes]
|}

[[Category:Firmware]]