<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/appletv16-beta.jinja -->
{{nobots}}
== [[Apple TV HD]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
! Download URL
! File Size
|-
| 16.0 beta
| 20J5299n
| [[Keys:ParisSeed 20J5299n (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|06|06}}
| [https://developer.apple.com/services-account/download?path=/WWDC_2022/tvOS_16_beta/AppleTV53_16.0_20J5299n_Restore.ipsw AppleTV53_16.0_20J5299n_Restore.ipsw]
| 3,549,708,887
|-
| 16.0 beta 2
| 20J5319h
| [[Keys:ParisSeed 20J5319h (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|06|22}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-30801/8496544E-3794-4361-AF13-F874BF7378AC/AppleTV5,3_16.0_20J5319h_Restore.ipsw AppleTV5,3_16.0_20J5319h_Restore.ipsw]
| 3,564,320,901
|-
| 16.0 beta 3
| 20J5328g
| [[Keys:ParisSeed 20J5328g (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|07|06}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-38097/0C36E160-B42A-4949-BDB1-0CE8A1F62AC6/AppleTV5,3_16.0_20J5328g_Restore.ipsw AppleTV5,3_16.0_20J5328g_Restore.ipsw]
| 3,510,920,004
|-
| 16.0 beta 4
| 20J5344f
| [[Keys:ParisSeed 20J5344f (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|07|27}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-43815/14244770-9B25-4187-8DE9-295325A03349/AppleTV5,3_16.0_20J5344f_Restore.ipsw AppleTV5,3_16.0_20J5344f_Restore.ipsw]
| 3,528,162,294
|-
| 16.0 beta 5
| 20J5355f
| [[Keys:ParisSeed 20J5355f (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|08|08}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-51664/2E1F872F-6754-4BAA-B746-E6FFB4F02FF2/AppleTV5,3_16.0_20J5355f_Restore.ipsw AppleTV5,3_16.0_20J5355f_Restore.ipsw]
| 3,541,267,015
|-
| 16.0 beta 6
| 20J5366a
| [[Keys:ParisSeed 20J5366a (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|08|15}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-55654/AD1A1EFB-22E4-4985-8C4B-6AA65D0AFBCC/AppleTV5,3_16.0_20J5366a_Restore.ipsw AppleTV5,3_16.0_20J5366a_Restore.ipsw]
| 3,540,506,388
|-
| 16.0 beta 7
| 20J5371a
| [[Keys:ParisSeed 20J5371a (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|08|23}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-56234/7D341758-F65D-4886-A374-F942A1F36DF5/AppleTV5,3_16.0_20J5371a_Restore.ipsw AppleTV5,3_16.0_20J5371a_Restore.ipsw]
| 3,541,233,590
|-
| 16.0 [[Release Candidate|RC]]
| 20J373
| [[Keys:Paris 20J373 (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|09|07}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/071-16071/EF103FFF-EB65-4CFF-B560-01556A8BF483/AppleTV5,3_16.0_20J373_Restore.ipsw AppleTV5,3_16.0_20J373_Restore.ipsw]
| 3,556,818,318
|-
| 16.1 beta
| 20K5041d
| [[Keys:ParisBSeed 20K5041d (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|09|14}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-55172/A3DBF558-EFAC-4833-9409-085E7D18D1F4/AppleTV5,3_16.1_20K5041d_Restore.ipsw AppleTV5,3_16.1_20K5041d_Restore.ipsw]
| 3,632,782,521
|-
| 16.1 beta 2
| 20K5046d
| [[Keys:ParisBSeed 20K5046d (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|09|20}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-69570/C48FBAB3-3E8D-48E3-A937-9A4DD65BF2EE/AppleTV5,3_16.1_20K5046d_Restore.ipsw AppleTV5,3_16.1_20K5046d_Restore.ipsw]
| 3,637,064,791
|-
| 16.1 beta 3
| 20K5052c
| [[Keys:ParisBSeed 20K5052c (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|09|27}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-71578/F59F73E5-93E4-42BD-8CA7-908DFEAB7144/AppleTV5,3_16.1_20K5052c_Restore.ipsw AppleTV5,3_16.1_20K5052c_Restore.ipsw]
| 3,636,678,567
|-
| 16.1 beta 4
| 20K5062a
| [[Keys:ParisBSeed 20K5062a (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|10|04}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-82539/1E6FC619-3D4A-451F-A4EE-7CF10CE6D26F/AppleTV5,3_16.1_20K5062a_Restore.ipsw AppleTV5,3_16.1_20K5062a_Restore.ipsw]
| 3,640,319,146
|-
| 16.1 beta 5
| 20K5068a
| [[Keys:ParisBSeed 20K5068a (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|10|11}}
| [https://updates.cdn-apple.com/2022SummerSeed/fullrestores/012-88021/FA88A9CD-8C09-4F00-829F-B6D24CBC96BE/AppleTV5,3_16.1_20K5068a_Restore.ipsw AppleTV5,3_16.1_20K5068a_Restore.ipsw]
| 3,640,423,577
|-
| 16.1 [[Release Candidate|RC]]
| 20K71
| [[Keys:ParisB 20K71 (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|10|18}}
| [https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-37047/221EE266-BE62-456A-9A17-4C89A98A7E97/AppleTV5,3_16.1_20K71_Restore.ipsw AppleTV5,3_16.1_20K71_Restore.ipsw]
| 3,645,360,357
|-
| 16.2 beta
| 20K5331f
| [[Keys:ParisCSeed 20K5331f (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|10|25}}
| [https://updates.cdn-apple.com/2022FallSeed/fullrestores/012-81981/29B26552-DB09-4D72-8952-03609AAA9342/AppleTV5,3_16.2_20K5331f_Restore.ipsw AppleTV5,3_16.2_20K5331f_Restore.ipsw]
| 3,699,668,938
|-
| 16.2 beta 2
| 20K5342d
| [[Keys:ParisCSeed 20K5342d (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|11|08}}
| [https://updates.cdn-apple.com/2022FallSeed/fullrestores/012-98360/AFF9D8EA-E2E5-4B22-9889-D4C7F134F4B8/AppleTV5,3_16.2_20K5342d_Restore.ipsw AppleTV5,3_16.2_20K5342d_Restore.ipsw]
| 3,707,634,252
|-
| 16.2 beta 3
| 20K5348d
| [[Keys:ParisCSeed 20K5348d (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|11|15}}
| [https://updates.cdn-apple.com/2022FallSeed/fullrestores/032-06235/31A59356-0D8B-4413-BD4B-EC975E94224E/AppleTV5,3_16.2_20K5348d_Restore.ipsw AppleTV5,3_16.2_20K5348d_Restore.ipsw]
| 3,706,271,298
|-
| 16.2 beta 4
| 20K5357b
| [[Keys:ParisCSeed 20K5357b (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|12|01}}
| [https://updates.cdn-apple.com/2022FallSeed/fullrestores/032-07487/1C95BDE3-698B-4E95-B094-DB1E9816E281/AppleTV5,3_16.2_20K5357b_Restore.ipsw AppleTV5,3_16.2_20K5357b_Restore.ipsw]
| 3,706,327,988
|-
| 16.2 [[Release Candidate|RC]]
| 20K362
| [[Keys:ParisC 20K362 (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|12|07}}
| [https://updates.cdn-apple.com/2022WinterFCS/fullrestores/012-62470/AF38755F-FBD2-44D5-A48E-C3E6A66F94BD/AppleTV5,3_16.2_20K362_Restore.ipsw AppleTV5,3_16.2_20K362_Restore.ipsw]
| 3,709,975,754
|-
| 16.3 beta
| 20K5626c
| [[Keys:ParisDSeed 20K5626c (AppleTV5,3)|AppleTV5,3]]
| {{date|2022|12|14}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-09490/75E7BE07-7E36-4843-A42A-736879F11349/AppleTV5,3_16.3_20K5626c_Restore.ipsw AppleTV5,3_16.3_20K5626c_Restore.ipsw]
| 3,710,682,886
|-
| 16.3 beta 2
| 20K5637g
| [[Keys:ParisDSeed 20K5637g (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|01|10}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-33162/B8999AB2-4D65-49E4-B788-3B99D622BB90/AppleTV5,3_16.3_20K5637g_Restore.ipsw AppleTV5,3_16.3_20K5637g_Restore.ipsw]
| 3,707,832,689
|-
| 16.3 [[Release Candidate|RC]]
| 20K650
| [[Keys:ParisD 20K650 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|01|18}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-35546/0AA6B88D-D66B-4095-A19B-5831013D46C7/AppleTV5,3_16.3_20K650_Restore.ipsw AppleTV5,3_16.3_20K650_Restore.ipsw]
| 3,778,378,139
|-
| 16.4 beta
| 20L5463g
| [[Keys:ParisESeed 20L5463g (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|02|16}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-50982/06156B89-0DC8-4386-89B8-FB61898C59C3/AppleTV5,3_16.4_20L5463g_Restore.ipsw AppleTV5,3_16.4_20L5463g_Restore.ipsw]
| 3,859,702,576
|-
| 16.4 beta 2
| 20L5474e
| [[Keys:ParisESeed 20L5474e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|02|28}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-54114/6EE117DA-0515-4E4A-A5DD-B73227EC204E/AppleTV5,3_16.4_20L5474e_Restore.ipsw AppleTV5,3_16.4_20L5474e_Restore.ipsw]
| 3,861,528,402
|-
| 16.4 beta 3
| 20L5480g
| [[Keys:ParisESeed 20L5480g (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|03|07}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-60371/D94726E4-4085-499F-AFD0-8EE835CC3E10/AppleTV5,3_16.4_20L5480g_Restore.ipsw AppleTV5,3_16.4_20L5480g_Restore.ipsw]
| 3,860,523,309
|-
| 16.4 beta 4
| 20L5490a
| [[Keys:ParisESeed 20L5490a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|03|14}}
| [https://updates.cdn-apple.com/2023WinterSeed/fullrestores/032-62038/75D7ECE9-462B-4FA8-A07B-CFA3CBC76755/AppleTV5,3_16.4_20L5490a_Restore.ipsw AppleTV5,3_16.4_20L5490a_Restore.ipsw]
| 3,863,958,058
|-
| 16.4 [[Release Candidate|RC]]
| 20L497
| [[Keys:ParisE 20L497 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|03|21}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/032-65974/4E98239D-91B5-4485-8D1E-ACAF38003F39/AppleTV5,3_16.4_20L497_Restore.ipsw AppleTV5,3_16.4_20L497_Restore.ipsw]
| 3,865,509,707
|-
| 16.5 beta
| 20L5527d
| [[Keys:ParisFSeed 20L5527d (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|03|28}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-58721/5F73FD42-3A90-45E8-868B-FA479AF76C5F/AppleTV5,3_16.5_20L5527d_Restore.ipsw AppleTV5,3_16.5_20L5527d_Restore.ipsw]
| 3,871,555,172
|-
| 16.5 beta 2
| 20L5538d
| [[Keys:ParisFSeed 20L5538d (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|04|11}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-69646/5E4A2D54-9C22-44ED-9120-B1785ADC071C/AppleTV5,3_16.5_20L5538d_Restore.ipsw AppleTV5,3_16.5_20L5538d_Restore.ipsw]
| 3,869,185,855
|-
| 16.5 beta 3
| 20L5549e
| [[Keys:ParisFSeed 20L5549e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|04|25}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-76500/BF05C7F1-0EFD-4EB2-92DB-F2F8901CA66B/AppleTV5,3_16.5_20L5549e_Restore.ipsw AppleTV5,3_16.5_20L5549e_Restore.ipsw]
| 3,869,019,645
|-
| 16.5 beta 4
| 20L5559a
| [[Keys:ParisFSeed 20L5559a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|05|02}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-79655/B05C63A4-351A-4C89-9F8F-A9DDA98E587E/AppleTV5,3_16.5_20L5559a_Restore.ipsw AppleTV5,3_16.5_20L5559a_Restore.ipsw]
| 3,868,999,746
|-
| 16.5 [[Release Candidate|RC]]
| 20L562
| [[Keys:ParisF 20L562 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|05|09}}
| [https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-38690/B01FD24E-05D3-4484-B2F3-72366BD7C4B2/AppleTV5,3_16.5_20L562_Restore.ipsw AppleTV5,3_16.5_20L562_Restore.ipsw]
| 3,872,755,135
|-
| 16.6 beta
| 20M5527e
| [[Keys:ParisGSeed 20M5527e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|05|19}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-77404/A612ECEB-7F67-477C-9B77-25B43ADD04F9/AppleTV5,3_16.6_20M5527e_Restore.ipsw AppleTV5,3_16.6_20M5527e_Restore.ipsw]
| 3,887,497,392
|-
| 16.6 beta 2
| 20M5538d
| [[Keys:ParisGSeed 20M5538d (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|05|31}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-87981/A8EC7365-917F-4F46-9496-D8E6781793A4/AppleTV5,3_16.6_20M5538d_Restore.ipsw AppleTV5,3_16.6_20M5538d_Restore.ipsw]
| 3,886,721,324
|-
| 16.6 beta 3
| 20M5548b
| [[Keys:ParisGSeed 20M5548b (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|06|15}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/032-93137/B6A3FABD-9517-4153-9E81-50CE224770E3/AppleTV5,3_16.6_20M5548b_Restore.ipsw AppleTV5,3_16.6_20M5548b_Restore.ipsw]
| 3,889,302,581
|-
| 16.6 beta 4
| 20M5559c
| [[Keys:ParisGSeed 20M5559c (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|06|27}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/042-03576/A22AFE80-BCBB-4E9C-B763-21AEE9CF2810/AppleTV5,3_16.6_20M5559c_Restore.ipsw AppleTV5,3_16.6_20M5559c_Restore.ipsw]
| 3,888,441,588
|-
| 16.6 beta 5
| 20M5571a
| [[Keys:ParisGSeed 20M5571a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|07|10}}
| [https://updates.cdn-apple.com/2023SpringSeed/fullrestores/042-09534/D51D7C29-83D6-4570-9B42-44E3A99C1C37/AppleTV5,3_16.6_20M5571a_Restore.ipsw AppleTV5,3_16.6_20M5571a_Restore.ipsw]
| 3,886,960,687
|-
| 16.6 [[Release Candidate|RC]]
| 20M73
| [[Keys:ParisG 20M73 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|07|18}}
| [https://updates.cdn-apple.com/2023SummerFCS/fullrestores/042-18272/3CE91CB7-2332-42B7-AEF8-37478A767E90/AppleTV5,3_16.6_20M73_Restore.ipsw AppleTV5,3_16.6_20M73_Restore.ipsw]
| 3,887,550,686
|}

== [[Apple TV 4K]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 16.0 beta
| 20J5299n
| [[Keys:ParisSeed 20J5299n (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|06|06}}
|-
| 16.0 beta 2
| 20J5319h
| [[Keys:ParisSeed 20J5319h (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|06|22}}
|-
| 16.0 beta 3
| 20J5328g
| [[Keys:ParisSeed 20J5328g (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|07|06}}
|-
| 16.0 beta 4
| 20J5344f
| [[Keys:ParisSeed 20J5344f (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|07|27}}
|-
| 16.0 beta 5
| 20J5355f
| [[Keys:ParisSeed 20J5355f (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|08|08}}
|-
| 16.0 beta 6
| 20J5366a
| [[Keys:ParisSeed 20J5366a (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|08|15}}
|-
| 16.0 beta 7
| 20J5371a
| [[Keys:ParisSeed 20J5371a (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|08|23}}
|-
| 16.0 [[Release Candidate|RC]]
| 20J373
| [[Keys:Paris 20J373 (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|09|07}}
|-
| 16.1 beta
| 20K5041d
| [[Keys:ParisBSeed 20K5041d (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|09|14}}
|-
| 16.1 beta 2
| 20K5046d
| [[Keys:ParisBSeed 20K5046d (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|09|20}}
|-
| 16.1 beta 3
| 20K5052c
| [[Keys:ParisBSeed 20K5052c (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|09|27}}
|-
| 16.1 beta 4
| 20K5062a
| [[Keys:ParisBSeed 20K5062a (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|10|04}}
|-
| 16.1 beta 5
| 20K5068a
| [[Keys:ParisBSeed 20K5068a (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|10|11}}
|-
| 16.1 [[Release Candidate|RC]]
| 20K71
| [[Keys:ParisB 20K71 (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|10|18}}
|-
| 16.2 beta
| 20K5331f
| [[Keys:ParisCSeed 20K5331f (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|10|25}}
|-
| 16.2 beta 2
| 20K5342d
| [[Keys:ParisCSeed 20K5342d (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|11|08}}
|-
| 16.2 beta 3
| 20K5348d
| [[Keys:ParisCSeed 20K5348d (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|11|15}}
|-
| 16.2 beta 4
| 20K5357b
| [[Keys:ParisCSeed 20K5357b (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|12|01}}
|-
| 16.2 [[Release Candidate|RC]]
| 20K362
| [[Keys:ParisC 20K362 (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|12|07}}
|-
| 16.3 beta
| 20K5626c
| [[Keys:ParisDSeed 20K5626c (AppleTV6,2)|AppleTV6,2]]
| {{date|2022|12|14}}
|-
| 16.3 beta 2
| 20K5637g
| [[Keys:ParisDSeed 20K5637g (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|01|10}}
|-
| 16.3 [[Release Candidate|RC]]
| 20K650
| [[Keys:ParisD 20K650 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|01|18}}
|-
| 16.4 beta
| 20L5463g
| [[Keys:ParisESeed 20L5463g (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|02|16}}
|-
| 16.4 beta 2
| 20L5474e
| [[Keys:ParisESeed 20L5474e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|02|28}}
|-
| 16.4 beta 3
| 20L5480g
| [[Keys:ParisESeed 20L5480g (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|03|07}}
|-
| 16.4 beta 4
| 20L5490a
| [[Keys:ParisESeed 20L5490a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|03|14}}
|-
| 16.4 [[Release Candidate|RC]]
| 20L497
| [[Keys:ParisE 20L497 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|03|21}}
|-
| 16.5 beta
| 20L5527d
| [[Keys:ParisFSeed 20L5527d (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|03|28}}
|-
| 16.5 beta 2
| 20L5538d
| [[Keys:ParisFSeed 20L5538d (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|04|11}}
|-
| 16.5 beta 3
| 20L5549e
| [[Keys:ParisFSeed 20L5549e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|04|25}}
|-
| 16.5 beta 4
| 20L5559a
| [[Keys:ParisFSeed 20L5559a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|05|02}}
|-
| 16.5 [[Release Candidate|RC]]
| 20L562
| [[Keys:ParisF 20L562 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|05|09}}
|-
| 16.6 beta
| 20M5527e
| [[Keys:ParisGSeed 20M5527e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|05|19}}
|-
| 16.6 beta 2
| 20M5538d
| [[Keys:ParisGSeed 20M5538d (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|05|31}}
|-
| 16.6 beta 3
| 20M5548b
| [[Keys:ParisGSeed 20M5548b (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|06|15}}
|-
| 16.6 beta 4
| 20M5559c
| [[Keys:ParisGSeed 20M5559c (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|06|27}}
|-
| 16.6 beta 5
| 20M5571a
| [[Keys:ParisGSeed 20M5571a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|07|10}}
|-
| 16.6 [[Release Candidate|RC]]
| 20M73
| [[Keys:ParisG 20M73 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|07|18}}
|}

== [[Apple TV 4K (2nd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 16.0 beta
| 20J5299n
| [[Keys:ParisSeed 20J5299n (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|06|06}}
|-
| 16.0 beta 2
| 20J5319h
| [[Keys:ParisSeed 20J5319h (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|06|22}}
|-
| 16.0 beta 3
| 20J5328g
| [[Keys:ParisSeed 20J5328g (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|07|06}}
|-
| 16.0 beta 4
| 20J5344f
| [[Keys:ParisSeed 20J5344f (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|07|27}}
|-
| 16.0 beta 5
| 20J5355f
| [[Keys:ParisSeed 20J5355f (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|08|08}}
|-
| 16.0 beta 6
| 20J5366a
| [[Keys:ParisSeed 20J5366a (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|08|15}}
|-
| 16.0 beta 7
| 20J5371a
| [[Keys:ParisSeed 20J5371a (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|08|23}}
|-
| 16.0 [[Release Candidate|RC]]
| 20J373
| [[Keys:Paris 20J373 (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|09|07}}
|-
| 16.1 beta
| 20K5041d
| [[Keys:ParisBSeed 20K5041d (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|09|14}}
|-
| 16.1 beta 2
| 20K5046d
| [[Keys:ParisBSeed 20K5046d (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|09|20}}
|-
| 16.1 beta 3
| 20K5052c
| [[Keys:ParisBSeed 20K5052c (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|09|27}}
|-
| 16.1 beta 4
| 20K5062a
| [[Keys:ParisBSeed 20K5062a (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|10|04}}
|-
| 16.1 beta 5
| 20K5068a
| [[Keys:ParisBSeed 20K5068a (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|10|11}}
|-
| 16.1 [[Release Candidate|RC]]
| 20K71
| [[Keys:ParisB 20K71 (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|10|18}}
|-
| 16.2 beta
| 20K5331f
| [[Keys:ParisCSeed 20K5331f (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|10|25}}
|-
| 16.2 beta 2
| 20K5342d
| [[Keys:ParisCSeed 20K5342d (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|11|08}}
|-
| 16.2 beta 3
| 20K5348d
| [[Keys:ParisCSeed 20K5348d (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|11|15}}
|-
| 16.2 beta 4
| 20K5357b
| [[Keys:ParisCSeed 20K5357b (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|12|01}}
|-
| 16.2 [[Release Candidate|RC]]
| 20K362
| [[Keys:ParisC 20K362 (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|12|07}}
|-
| 16.3 beta
| 20K5626c
| [[Keys:ParisDSeed 20K5626c (AppleTV11,1)|AppleTV11,1]]
| {{date|2022|12|14}}
|-
| 16.3 beta 2
| 20K5637g
| [[Keys:ParisDSeed 20K5637g (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|01|10}}
|-
| 16.3 [[Release Candidate|RC]]
| 20K650
| [[Keys:ParisD 20K650 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|01|18}}
|-
| 16.4 beta
| 20L5463g
| [[Keys:ParisESeed 20L5463g (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|02|16}}
|-
| 16.4 beta 2
| 20L5474e
| [[Keys:ParisESeed 20L5474e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|02|28}}
|-
| 16.4 beta 3
| 20L5480g
| [[Keys:ParisESeed 20L5480g (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|03|07}}
|-
| 16.4 beta 4
| 20L5490a
| [[Keys:ParisESeed 20L5490a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|03|14}}
|-
| 16.4 [[Release Candidate|RC]]
| 20L497
| [[Keys:ParisE 20L497 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|03|21}}
|-
| 16.5 beta
| 20L5527d
| [[Keys:ParisFSeed 20L5527d (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|03|28}}
|-
| 16.5 beta 2
| 20L5538d
| [[Keys:ParisFSeed 20L5538d (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|04|11}}
|-
| 16.5 beta 3
| 20L5549e
| [[Keys:ParisFSeed 20L5549e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|04|25}}
|-
| 16.5 beta 4
| 20L5559a
| [[Keys:ParisFSeed 20L5559a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|05|02}}
|-
| 16.5 [[Release Candidate|RC]]
| 20L562
| [[Keys:ParisF 20L562 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|05|09}}
|-
| 16.6 beta
| 20M5527e
| [[Keys:ParisGSeed 20M5527e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|05|19}}
|-
| 16.6 beta 2
| 20M5538d
| [[Keys:ParisGSeed 20M5538d (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|05|31}}
|-
| 16.6 beta 3
| 20M5548b
| [[Keys:ParisGSeed 20M5548b (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|06|15}}
|-
| 16.6 beta 4
| 20M5559c
| [[Keys:ParisGSeed 20M5559c (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|06|27}}
|-
| 16.6 beta 5
| 20M5571a
| [[Keys:ParisGSeed 20M5571a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|07|10}}
|-
| 16.6 [[Release Candidate|RC]]
| 20M73
| [[Keys:ParisG 20M73 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|07|18}}
|}

== [[Apple TV 4K (3rd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 16.2 beta
| 20K5331f
| [[Keys:ParisCSeed 20K5331f (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|10|25}}
|-
| 16.2 beta 2
| 20K5342d
| [[Keys:ParisCSeed 20K5342d (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|11|08}}
|-
| 16.2 beta 3
| 20K5348d
| [[Keys:ParisCSeed 20K5348d (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|11|15}}
|-
| 16.2 beta 4
| 20K5357b
| [[Keys:ParisCSeed 20K5357b (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|12|01}}
|-
| 16.2 [[Release Candidate|RC]]
| 20K362
| [[Keys:ParisC 20K362 (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|12|07}}
|-
| 16.3 beta
| 20K5626c
| [[Keys:ParisDSeed 20K5626c (AppleTV14,1)|AppleTV14,1]]
| {{date|2022|12|14}}
|-
| 16.3 beta 2
| 20K5637g
| [[Keys:ParisDSeed 20K5637g (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|01|10}}
|-
| 16.3 [[Release Candidate|RC]]
| 20K650
| [[Keys:ParisD 20K650 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|01|18}}
|-
| 16.4 beta
| 20L5463g
| [[Keys:ParisESeed 20L5463g (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|02|16}}
|-
| 16.4 beta 2
| 20L5474e
| [[Keys:ParisESeed 20L5474e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|02|28}}
|-
| 16.4 beta 3
| 20L5480g
| [[Keys:ParisESeed 20L5480g (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|03|07}}
|-
| 16.4 beta 4
| 20L5490a
| [[Keys:ParisESeed 20L5490a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|03|14}}
|-
| 16.4 [[Release Candidate|RC]]
| 20L497
| [[Keys:ParisE 20L497 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|03|21}}
|-
| 16.5 beta
| 20L5527d
| [[Keys:ParisFSeed 20L5527d (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|03|28}}
|-
| 16.5 beta 2
| 20L5538d
| [[Keys:ParisFSeed 20L5538d (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|04|11}}
|-
| 16.5 beta 3
| 20L5549e
| [[Keys:ParisFSeed 20L5549e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|04|25}}
|-
| 16.5 beta 4
| 20L5559a
| [[Keys:ParisFSeed 20L5559a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|05|02}}
|-
| 16.5 [[Release Candidate|RC]]
| 20L562
| [[Keys:ParisF 20L562 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|05|09}}
|-
| 16.6 beta
| 20M5527e
| [[Keys:ParisGSeed 20M5527e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|05|19}}
|-
| 16.6 beta 2
| 20M5538d
| [[Keys:ParisGSeed 20M5538d (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|05|31}}
|-
| 16.6 beta 3
| 20M5548b
| [[Keys:ParisGSeed 20M5548b (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|06|15}}
|-
| 16.6 beta 4
| 20M5559c
| [[Keys:ParisGSeed 20M5559c (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|06|27}}
|-
| 16.6 beta 5
| 20M5571a
| [[Keys:ParisGSeed 20M5571a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|07|10}}
|-
| 16.6 [[Release Candidate|RC]]
| 20M73
| [[Keys:ParisG 20M73 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|07|18}}
|}

[[Category:Firmware]]