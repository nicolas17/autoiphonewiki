<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/homepod17-beta.jinja -->
{{nobots}}
== [[HomePod]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:StarlightSeed 21J5273q (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:StarlightSeed 21J5303f (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2023|07|05}}
|-
| 17.0 beta 3
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AudioAccessory1,1)|AudioAccessory1,1]]<br/>[[Keys:StarlightSeed 21J5318f (AudioAccessory1,2)|AudioAccessory1,2]]
| {{date|2023|07|25}}
|-
| 17.0 beta 4
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|08|08}}
|-
| 17.0 beta 5
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|08|15}}
|-
| 17.0 beta 6
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|08|22}}
|-
| 17.0 beta 7
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|08|29}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AudioAccessory1,1)|AudioAccessory1,1]]
| {{date|2024|03|04}}
|}

== [[HomePod mini]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|07|05}}
|-
| 17.0 beta 3
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|07|25}}
|-
| 17.0 beta 4
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|08|08}}
|-
| 17.0 beta 5
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|08|15}}
|-
| 17.0 beta 6
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|08|22}}
|-
| 17.0 beta 7
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|08|29}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AudioAccessory5,1)|AudioAccessory5,1]]
| {{date|2024|03|04}}
|}

== [[HomePod (2nd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|07|05}}
|-
| 17.0 beta 3
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|07|25}}
|-
| 17.0 beta 4
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|08|08}}
|-
| 17.0 beta 5
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|08|15}}
|-
| 17.0 beta 6
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|08|22}}
|-
| 17.0 beta 7
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|08|29}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AudioAccessory6,1)|AudioAccessory6,1]]
| {{date|2024|03|04}}
|}

[[Category:Firmware]]